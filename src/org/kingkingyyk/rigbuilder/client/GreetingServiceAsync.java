package org.kingkingyyk.rigbuilder.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface GreetingServiceAsync {
	void greetServer(String input, AsyncCallback<ArrayList<String>> callback) throws IllegalArgumentException;
}
